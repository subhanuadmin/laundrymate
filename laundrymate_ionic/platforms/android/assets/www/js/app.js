// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic',
                            'ngCordova',
                            'app.controllers',
                            'app.factories',
                            ])

.run(function($ionicPlatform, $ionicLoading, $rootScope, $state, Auth) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      // cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

  });

  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){

      if (toState.authRequired && !Auth.isAuthenticated()){ //Assuming the Auth holds authentication logic

        // User isn’t authenticated
        if(Auth.isFirstTimeUser()) {
            console.log('rootScope statechange intro');
            $state.go("intro");
        } else {
            console.log('rootScope statechange login');
            $state.go("login");
        }
        event.preventDefault();

    }

  });

  $rootScope.$on('loading:show', function() {
    $ionicLoading.show({template: 'Loading...'})
  })

  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide()
  })

})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.interceptors.push(function($rootScope) {
      return {
        request: function(config) {
          $rootScope.$broadcast('loading:show')
          return config
        },
        response: function(response) {
          $rootScope.$broadcast('loading:hide')
          return response
        }
      }
    })

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl',
    cache: false
  })

   .state('first', {
      url: '/first',
      templateUrl: 'templates/first.html',
      controller: 'FirstCtrl',
      authRequired: false
   })

  

  .state('login', {
    url: '/login/:message',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl',
    authRequired: false,
    cache: false
  })

  .state('signup', {
    url: '/signup',
    controller: 'SignupCtrl',
    templateUrl: 'templates/signup.html',
    authRequired: false,
    cache: false
  })

  .state('verifyotp', {
    url: '/verify-otp',
    controller: 'VerifyOTPCtrl',
    templateUrl: 'templates/verify_otp.html',
    authRequired: false,
    cache: false
  })

  .state('intro', {
    url: '/intro',
    controller: 'IntroCtrl',
    templateUrl: 'templates/intro.html',
    authRequired: false
  })

  .state('app.home', {
      url: '/wash-details/',
      views: {
        'menuContent': {
            templateUrl: 'templates/washDetails.html',
            controller: 'WashDetailsCtrl'
        }
      },
      authRequired: true,
      cache: false
  })


  .state('inviteandearn', {
    url: '/invite-and-earn/',
    controller: 'InviteEarnCtrl',
    templateUrl: 'templates/invite_and_earn.html',
    authRequired: false,
    cache: false
})

  .state('referralinfo', {
      url: '/referral-info/',
      controller: 'ReferralInfoCtrl',
      templateUrl: 'templates/referralinfo.html',
      authRequired: false,
      cache: false
  })

  .state('offersinfo', {
      url: '/offers-info/',
      controller: 'OffersInfoCtrl',
      templateUrl: 'templates/referralinfo.html',
      authRequired: false,
      cache: false
  })

  .state('howwework', {
      url: '/howwework/',
      controller: 'HowWeWorkCtrl',
      templateUrl: 'templates/howwework.html',
      authRequired: false,
      cache: false
  })

   .state('pricelistinfo', {
      url: '/pricelistinfo/',
      controller: 'PriceListInfoCtrl',
      templateUrl: 'templates/ourpricing2.html',
      authRequired: false,
      cache: false
  })

   .state('app.newhome', {
    url: '/newhome',
     views: {
        'menuContent': {
            templateUrl: 'templates/newhome.html',
            controller: 'NewHomeCtrl'
        }
      },
    authRequired: true,
    cache: false
  })

   .state('guesthome', {
    url: '/guesthome',
    controller: 'NewHomeCtrl',
    templateUrl: 'templates/guesthomenew.html',
    authRequired: false,
    cache: false
  })

  .state('app.pricing', {
      url: '/pricing/',
      views: {
        'menuContent': {
            templateUrl: 'templates/pricing.html',
            controller: 'PricingCtrl'
        }
      },
      authRequired: true,
      cache: false
  })

  .state('app.washDetails', {
      url: '/wash-details/{washType}',
      views: {
        'menuContent': {
            templateUrl: 'templates/washDetails.html',
            controller: 'WashDetailsCtrl'
        }
      },
      authRequired: true,
      cache: false
  })

  .state('app.addons', {
      url: '/addons/{washType}',
      views: {
        'menuContent': {
            templateUrl: 'templates/addons.html',
            controller: 'AddonsCtrl'
        }
      },
      authRequired: true,
      cache: false
  })

  .state('app.washSchedule', {
    url: '/wash-schedule/{washType}',
    views: {
      'menuContent': {
        templateUrl: 'templates/washSchedule.html',
        controller: 'WashScheduleCtrl'
      }
    },
    authRequired: true,
    cache: false
  })

  .state('app.payment', {
    url: '/payment',
    views: {
      'menuContent': {
        templateUrl: 'templates/payment.html',
        controller: 'PaymentCtrl'
      }
    },
    authRequired: true,
    cache: false
  })

  .state('app.orderReview', {
    url: '/order-review/{paymentMethod}',
    views: {
      'menuContent': {
        templateUrl: 'templates/orderReview.html',
        controller: 'OrderReviewCtrl'
      }
    },
    authRequired: true,
    cache: false
  })

  .state('app.orderDetail', {
    url: '/order-detail/:orderId',
    views: {
      'menuContent': {
        templateUrl: 'templates/orderDetails.html',
        controller: 'OrderDetailCtrl'
      }
    },
    authRequired: true,
    cache: false
  })

  .state('app.profile', {
      url: '/profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile.html',
          controller: 'ProfileCtrl'
        }
      },
      authRequired: true,
      cache: false
  })

  .state('app.manageAddress', {
      url: '/manage-address',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageAddress.html',
          controller: 'ManageAddressCtrl'
        }
      },
      authRequired: true,
      cache: false
  })

  .state('app.mywallet', {
    url: '/wallet',
    views: {
      'menuContent': {
        templateUrl: 'templates/wallet.html',
        controller: 'WalletCtrl'
      }
    },
    authRequired: true,
    cache: false
  })

  .state('app.orders', {
        url: '/orders',
        views: {
          'menuContent': {
            templateUrl: 'templates/myOrders.html',
            controller: 'OrdersCtrl'
          }
        },
        authRequired: true,
        cache: false
   })

   .state('app.faqs', {
         url: '/faqs',
         views: {
           'menuContent': {
             templateUrl: 'templates/flatpage.html',
             controller: 'FAQCtrl'
           }
         },
         authRequired: false,
    })

    .state('app.contactUs', {
          url: '/contact',
          views: {
            'menuContent': {
              templateUrl: 'templates/flatpage.html',
              controller: 'ContactUsCtrl'
            }
          },
          authRequired: false,
     })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/intro');
});
