Object.toparams = function ObjecttoParams(obj)
{
  var p = [];
  for (var key in obj)
  {
    p.push(key + '=' + encodeURIComponent(obj[key]));
  }
  return p.join('&');
};

angular.module('app.factories', [])

.factory('$localStorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
    removeItem: function(key){
        $window.localStorage.removeItem(key);
    }
  }
}])

.factory('Auth', function ($localStorage) {

   var TOKEN_KEY = "lm._token";
   var USER_KEY = "lm._user";
   var FIRST_TIME_KEY = "lm._isFirstTime";

   return {

      isAuthenticated: function () {
          _token = $localStorage.get(TOKEN_KEY);
          _user = $localStorage.get(USER_KEY);
          if(typeof(_token) == 'undefined' || typeof(_user) == 'undefined') {
             return false;
          }
          return true;
      },

      isFirstTimeUser: function(){
        var visitor = $localStorage.getObject(FIRST_TIME_KEY);
        return angular.equals(visitor, {});
      },

      getUser: function () {
         if($localStorage.getObject(USER_KEY)) {
            return $localStorage.getObject(USER_KEY);
         } else {
            return {}
         }
      },

      updateUser: function(user) {
          $localStorage.setObject(USER_KEY, user);
      },

      updateFirstTimeUser: function(){
          $localStorage.setObject(FIRST_TIME_KEY, {'visited': true});
      },

      updateToken: function(user, token){
          $localStorage.setObject(TOKEN_KEY, token);
          $localStorage.setObject(USER_KEY, user);
      },

      clearToken: function() {
          $localStorage.removeItem(TOKEN_KEY);
          $localStorage.removeItem(USER_KEY);
      },

      getToken: function(){
          return $localStorage.getObject(TOKEN_KEY);
      },

      logout: function () {
         $localStorage.removeItem(USER_KEY);
         $localStorage.removeItem(FIRST_TIME_KEY);
         _user = null;
         _isFirstTime = null;
      }
   }
})

.factory("LM", function($http, $localStorage, $q, Auth){

    var API_URL = "https://www.laundrymate.in";
    // var API_URL = "http://127.0.0.1:8000";

    return {

      getAPIURL: function(){
        return API_URL;
      },

      showInvoice: function(order_id) {
        var defer = $q.defer(); 
        $http({
          url: API_URL + '/mapi/invoice/'+order_id+'/show/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise; 

      },

      login: function (username, password) {
        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/login/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          data: Object.toparams({username: username, password: password})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      signup: function (formData) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/signup/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          data: Object.toparams(formData)
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      verifyOTP: function(otp){

          var defer = $q.defer();
          $http({
            url: API_URL + '/mapi/verify-otp/',
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: Object.toparams({otp: otp})
          })
            .success(function (resp) {
              defer.resolve(resp);
            })
            .error(function (error) {
              defer.reject(error);
            });

          return defer.promise;

      },

       getBanners: function(){

          var defer = $q.defer();
          $http({
            url: API_URL + '/mapi/get-banners/',
            method: 'GET',
            headers: {
            },
          })
            .success(function (resp) {
              defer.resolve(resp);
            })
            .error(function (error) {
              defer.reject(error);
            });

          return defer.promise;

      },

      getReferalLink: function(){
        
          var defer = $q.defer();
          $http({
            url: API_URL + '/mapi/get-referal-link/',
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
              'Authorization': 'Token ' + Auth.getToken()
            },
          })
            .success(function (resp) {
              defer.resolve(resp);
            })
            .error(function (error) {
              defer.reject(error);
            });

          return defer.promise;
        
      },

      getPaymentMessage: function(){

          var defer = $q.defer();
          $http({
            url: API_URL + '/mapi/get-payment-message/',
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
              'Authorization': 'Token ' + Auth.getToken()
            },
            data: Object.toparams({})
          })
            .success(function (resp) {
              defer.resolve(resp);
            })
            .error(function (error) {
              defer.reject(error);
            });

          return defer.promise;

      },


      getWashFormData: function (washType) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-wash-details/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({'wash_type': washType})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;
      },

      getSlots: function (washType) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-slots/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({washType: washType})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;
      },

      createOrder: function (paymentMethod) {

        var defer = $q.defer();

        $http({
          url: API_URL + '/mapi/create-order/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({payment_method: paymentMethod})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;


      },

      getOrderDetails: function (orderId) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-order-details/' + orderId + '/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({
            order_details: JSON.stringify($localStorage.getObject('lm.order.details').price_qty),
            order_slots: JSON.stringify($localStorage.getObject('lm.order.slots'))
          })
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;

      },

      getCurrentOrder: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-current-order/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: {}
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;


      },

      getAddons: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-addons/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: {}
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;


      },

      postAddons: function (addons) {

        console.log(addons);

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/post-addons/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({addons: JSON.stringify(addons)})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;


      },

      createAddress: function (address) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/save-address/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams(address)
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;

      },

      updateProfile: function (user) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/update-profile/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams(user)
        })
          .success(function (resp) {

            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;

      },

      postWashDetails: function (washType) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/post-wash-details/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({wash_type: washType})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;


      },

      postSlotDetails: function (slotDetails, special_instructions) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/post-slot-details/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({slot_details: JSON.stringify(slotDetails), special_instructions: special_instructions})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;

      },

      applyCoupon: function (coupon) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/apply-coupon/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({coupon: coupon})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });


        return defer.promise;

      },

      getActiveOrders: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-active-orders/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: {}
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      getOrders: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-orders/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: {}
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      getAddress: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-address/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: {}
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      updateAddress: function (address) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/update-address/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams(address)
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      getPricing: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-pricing/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          },
          data: {}
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      getFlatPage: function (page) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-flatpage/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          },
          data: Object.toparams({'page': page})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      getOrderQuantitySummary: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-order-quantity-summary/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      updateOrderQuantity: function (pricing_id, quantity) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/update-quantity/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({'pricing_id': pricing_id, 'quantity': quantity})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      cancelOrder: function (order_id) {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/cancel-order/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({'order_id': order_id})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      },

      getWalletDetails: function () {

        var defer = $q.defer();
        $http({
          url: API_URL + '/mapi/get-wallet-details/',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Authorization': 'Token ' + Auth.getToken()
          },
          data: Object.toparams({})
        })
          .success(function (resp) {
            defer.resolve(resp);
          })
          .error(function (error) {
            defer.reject(error);
          });

        return defer.promise;

      }


    }
});
