angular.module('app.controllers', [])

.controller('AppCtrl', function($scope,
                                $state,
                                $ionicModal,
                                $ionicHistory,
                                $localStorage,
                                Auth) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  if(Auth.isAuthenticated()) {
      $scope.user = Auth.getUser();
  }

  $scope.scheduleWash = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
    $state.go('app.washDetails', {washType: 'schedule'}, {location: 'replace'});
  };

  $scope.quickWash = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
    $state.go('app.washDetails', {washType: 'quick'}, {location: 'replace'});
  };

  $scope.home = function() {
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
    $state.go('app.newhome');
  };

  $scope.logout = function() {
      Auth.clearToken();
      $state.go('intro');
  };

  $scope.profile = function() {
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
      $state.go('app.profile');
  }

  $scope.orders = function(){
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
      $state.go('app.orders');
  }

  $scope.pricing = function() {
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
      $state.go('app.pricing');
  }

  $scope.faqs = function() {
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
      $state.go('app.faqs');
  }

  $scope.contactUs = function(){
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
      $state.go('app.contactUs');
  }

})

.controller('VerifyOTPCtrl', function($scope, $state, $ionicPopup, LM){

    $scope.form = {
        otp: ''
    };

    $scope.verify = function() {
        LM.verifyOTP($scope.form.otp)
        .then(function(resp){
            if(resp.success) {
                $state.go('login', {'message': 'Successfully Verified OTP.'});
            } else {
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Error verifying OTP. Please try again.'
                });
            }
        });
    }

})

.controller('PricingCtrl', function($scope, $state, $ionicHistory, LM) {

    LM.getPricing()
    .then(function(resp){
        $scope.pricing = resp.pricing;
    })

    $scope.gotoHome = function(){
        $ionicHistory.nextViewOptions({
          disableAnimate: true,
          disableBack: true
        });
        $state.go('app.home');
    }


})

.controller("FAQCtrl", function($scope, $sce, LM){

    LM.getFlatPage("faqs")
    .then(function(resp){
        $scope.title = resp.title;
        $scope.content = $sce.trustAsHtml(resp.content);
    })

})

.controller("ReferralInfoCtrl", function($scope, $sce,  $ionicHistory, LM){

    LM.getFlatPage("referral-faqs")
    .then(function(resp){
        $scope.title = resp.title;
        $scope.content = $sce.trustAsHtml(resp.content);
    });

     $scope.goBack = function() {
        $ionicHistory.goBack();
    };


})

.controller("PriceListInfoCtrl", function($scope, $state, $sce,  $ionicHistory, LM, Auth){

    $scope.goBack = function() {
        if(Auth.isAuthenticated()) {
            $state.go('app.newhome');
        } else {
            $state.go('guesthome'); 
        }
    };

     LM.getPricing()
    .then(function(resp){
        $scope.pricing = resp.pricing;
    })

})

.controller("HowWeWorkCtrl", function($scope, $sce,  $ionicHistory, LM){

  $scope.goBack = function() {
        $ionicHistory.goBack();
    };

})

.controller("OffersInfoCtrl", function($scope, $sce,  $ionicHistory, LM){

    LM.getFlatPage("offers")
    .then(function(resp){
        $scope.title = resp.title;
        $scope.content = $sce.trustAsHtml(resp.content);
    });

     $scope.goBack = function() {
        $ionicHistory.goBack();
    };


})


.controller("ContactUsCtrl", function($scope,  LM){

    LM.getFlatPage("contact")
    .then(function(resp){
        $scope.title = resp.title;
        $scope.content = resp.content;
    })

})

  .controller('OrdersCtrl', function($scope, $ionicPopup, $window, LM, Auth){

    $scope.activeTab = 'active';

    $scope.setTab = function(tab) {
        $scope.activeTab = tab;
    }

    $scope.active_orders = [];
    $scope.orders_history = [];

    LM.getOrders()
        .then(function(resp){
            $scope.active_orders = resp.active_orders;
            $scope.orders_history = resp.orders_history;
        });

    $scope.viewOrderDetails = function(order_id) {
        var opts = { toolbar: 'no', location: 'no', useWideViewPort: 'no', enableViewportScale: 'yes' };
        var invoice_url = LM.getAPIURL() + "/order/" + order_id + "/invoice/?token="+Auth.getToken();
        window.open(invoice_url, '_self', opts);
    }

    $scope.cancelOrder = function(order_id){

        var confirmPopup = $ionicPopup.confirm({
          title: 'Cancel Order',
          template: 'Are you sure you want to cancel this order?',
          cancelText: 'Dismiss',
          okText: 'Yes, Cancel the Order'
        });

        confirmPopup.then(function(res) {
          if(res) {
            LM.cancelOrder(order_id)
              .then(function(resp){
                if(resp.success){
                    $ionicPopup.alert({
                      title: 'Success',
                      template: 'Successfully cancelled order number:' + resp.order_number
                    });
                    $window.location.reload(true)
                } else {
                    $ionicPopup.alert({
                      title: 'Error',
                      template: 'Error cancelling order. Please try again.'
                    });
                }
              })
          } else {

          }
        });


    }

})

.controller('ProfileCtrl', function($scope, $state, $ionicModal, $ionicHistory, Auth, LM){

    $scope.user = Auth.getUser();
    $scope.user_errors = {};

    $ionicModal.fromTemplateUrl('templates/editProfile.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.updateProfile = function() {
        LM.updateProfile($scope.user)
          .then(function(resp){
              if(resp.success) {
                $scope.user = resp.user;
                Auth.updateUser(resp.user);
                $scope.closeModal();
              } else {
                $scope.openModal();
                $scope.user_errors = resp.errors;
              }
          })
    }

    $scope.manageAddress = function(){
        $state.go('app.manageAddress');
    }

    $scope.mywallet = function(){
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.mywallet');
    }

    $scope.editProfile = function() {
        $scope.modal.show();
    }

    $scope.openModal = function(){
      $scope.modal.show();
    };

    $scope.closeModal = function(){
      $scope.modal.hide();
    };

})

.controller('FirstCtrl', function($scope, $state, Auth){

  if(Auth.isFirstTimeUser()) {
    $state.go('intro');
  }

  $scope.signup = function() {
    $state.go('signup');
  };

  $scope.login = function() {
    $state.go('login', {'message': ''});
  };


})

.controller('InviteEarnCtrl', function($scope, $state, $ionicHistory, $cordovaEmailComposer, LM) {

    $scope.fb_url = "";
    $scope.twttier_url = "";
    $scope.email_link = "";

    LM.getReferalLink().then(function(res){
        if(res.success){
            $scope.referral_link = res.referral_link;
            console.log($scope.referral_link);
            $scope.fb_url = "https://www.facebook.com/sharer/sharer.php?u="+$scope.referral_link;
            $scope.twitter_url = "https://twitter.com/intent/tweet?url="+$scope.referral_link+"&text=Laundrymate%20Referral%20Offer";
            $scope.email_link = "mailto:?subject=Laundrymate%20Referral%20Offer&body=Hi,I found this website and thought you might like it "+$scope.referral_link;
        }
    });


    $scope.goBack = function() {
        $state.go('app.newhome');
    };

    $scope.openEmailComposer = function(){
        var email = {
            to: '',
            subject: 'Laundrymate Referral Offer',
            body: 'Hi, I found this website and thought you might like it: ' + $scope.referral_link,
            isHtml: true
          };
        
         $cordovaEmailComposer.open(email).then(null, function () {
           // user cancelled email
         });
    }

})

.controller('NewHomeCtrl', function($scope, $state, Auth, LM){

    $scope.banners = false;
    $scope.guest = true;

    if(Auth.isAuthenticated()) {
        $scope.guest = false;
    }

    $scope.options = {
        autoplay: 2000,
    }

    $scope.referralInfo = function(){
        // $state.go('referralinfo');
        $state.go('inviteandearn');
    }

    $scope.offersInfo = function(){
        $state.go('offersinfo');
    }

    $scope.howwework = function(){
        $state.go('howwework');
    }

    $scope.pricelistinfo = function(){
        $state.go('pricelistinfo');
    }

    $scope.gotoScheduleWash = function(){
        $state.go('app.washDetails');
    }

     $scope.gotoQuickWash = function(){
        $state.go('app.washDetails', {'washType': 'quick'});
    }

    $scope.signup = function() {
        $state.go('signup');
    };

    $scope.login = function() {
        $state.go('login', {'message': ''});
    };

    LM.getBanners()
        .then(function(resp) {
            $scope.banners = resp.banners;
        });

})

.controller('LoginCtrl', function($scope,
                                  $state,
                                  $stateParams,
                                  $ionicPopup,
                                  $ionicHistory,
                                  $rootScope,
                                  $localStorage,
                                  Auth,
                                  LM) {

  if(Auth.isFirstTimeUser()) {
      $state.go('intro');
  }

  $scope.form = {
      email: '',
      passwd: ''
  };

  $scope.errorsMessage = "";
  $scope.loginInDisabled = false;

  $scope.goBack = function(){

    if(Auth.isFirstTimeUser()) {
        $state.go('intro');
    }

    if(Auth.isAuthenticated()){
        $state.go('app.newhome');
    } else {
        $state.go('guesthome');
    }

  }

  $scope.login = function(){
      $scope.loginInDisabled = true;
      LM.login($scope.form.email, $scope.form.passwd)
        .then(function(resp){
          $scope.loginInDisabled = false;
          if(resp.success) {
            Auth.updateToken(resp.user, resp.token);
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
            $state.go('app.newhome');
          } else {
            $scope.errorMessage = resp.message;
          }
        }, function(error){
            $scope.errorMessage = error;
        })
  };

  $scope.signup = function() {
      $state.go('signup');
  };


  if($stateParams.message) {
    var alertPopup = $ionicPopup.alert({
      title: 'Message',
      template: $stateParams.message
    });
  }



})

.controller('IntroCtrl', function($scope, $state, Auth) {

  if(!Auth.isFirstTimeUser()) {

      if(Auth.isAuthenticated()) {
         $state.go('app.newhome');
      } else {
         $state.go('guesthome');
        // $state.go('login');
      }

  }

  $scope.signup = function() {
      Auth.updateFirstTimeUser();
      $state.go('signup');
  };

  $scope.login = function() {
      Auth.updateFirstTimeUser();
      $state.go('login', {'message': ''});
  };

})

.controller('SignupCtrl', function($scope, $state, LM) {

  $scope.signInDisabled = false;

  $scope.form = {
    email: '',
    password: '',
    repeat_password: '',
    full_name: '',
    mobile: 0,
    terms_condition: false,
    errors: {}
  };

  $scope.goBack = function(){
      $state.go('intro');
  }

  $scope.signup = function(){

    $scope.signInDisabled = true;

    LM.signup($scope.form)
      .then(function(resp){

        if(resp.success) {
            $state.go('verifyotp');
        }

        $scope.form.errors = resp.errors;
        $scope.signInDisabled = false;


      });

  }

})


.controller('WashDetailsCtrl', function($scope,
                                        $state,
                                        $stateParams,
                                        $ionicPopup,
                                        $ionicModal,
                                        LM,
                                        Auth, $localStorage) {

    $scope.activeTab = 'schedule';
    $scope.washType = $scope.activeTab;

    $scope.setTab = function(tab){
        $scope.activeTab = tab;
        $scope.washType = tab;
    }

    if($stateParams.washType == 'quick') {
        $scope.title = 'Quick Wash';
        $scope.washType = 'quick';
        $scope.activeTab = 'quick';
    } else {
        $scope.title = 'Schedule Wash';
        $scope.washType = 'schedule';
    }

    LM.getWashFormData($scope.washType)
      .then(function(resp){
        $scope.formData = resp;
        $scope.model.totalQuantity = resp.total_quantity;
        $scope.standard_delivery_charge = resp.standard_delivery_charge;
        $scope.quick_delivery_charge = resp.quick_delivery_charge;
     });

    $scope.model = {
        total: 0,
        totalQuantity: 0,
        sectionQuantities: []
    };

    $scope.serviceChange = function(){
        console.log('serviceChanged');
    };

    $scope.updateTotal = function() {

        var total = 0;

        for( var price_id in $scope.formData.price_qty) {
              total += $scope.formData.price_qty[price_id]['qty'] * $scope.formData.price_qty[price_id]['price'];
        }

        if($scope.formData.coupon != 0){
            total -= $scope.formData.coupon;
        }

        if($scope.formData.delivery_charge != 0){
            total += $scope.formData.delivery_charge;
        }

        $scope.model.total = total;


    };

    $scope.showQtyDetails = function(){

      LM.getOrderQuantitySummary()
        .then(function(resp){
            if(resp.success){
                $scope.modal = $ionicModal.fromTemplate(resp.modal_html, {
                    scope: $scope,
                    animation: 'slide-in-up'
                });
                $scope.modal.show();
            }
        });


    };

    $scope.closeModal = function(){
        $scope.modal.hide();
    },

    $scope.decr_qty = function(price_id, section) {

        var qty = $scope.formData.price_qty[price_id]['qty'];

        if(qty == 0) {
          return;
        }

        qty -= 1;

        if(qty <= 0) {
          qty = 0;
        }

        $scope.formData.price_qty[price_id]['qty'] = qty;

        LM.updateOrderQuantity(price_id, qty)
          .then(function(resp){
            if(resp.success) {
              $scope.model.totalQuantity = resp.order.total_quantity;
            } else {
                 $ionicPopup.alert({
                  title: 'Error',
                  template: resp.message
                });
            }
          });


    };

    $scope.quantityLimitCheck = function() {

        if($scope.washType == 'quick' && $scope.model.totalQuantity  >= $scope.formData['express_quantity_limit']) {
            return true;
        } else {
            return false;
        }

    }

    $scope.incr_qty = function(price_id, section) {
        var qty = $scope.formData.price_qty[price_id]['qty'];
        qty += 1;

        if($scope.quantityLimitCheck()) {
            $ionicPopup.alert({
                title: 'Error',
                template: 'Maximum quantity allowed is: ' + $scope.formData['express_quantity_limit']
              });
            // $scope.model.totalQuantity = $scope.formData['express_quantity_limit'];
            return;
        }

        $scope.formData.price_qty[price_id]['qty'] = qty;

        LM.updateOrderQuantity(price_id, qty)
          .then(function(resp){
            if(resp.success) {
              $scope.model.totalQuantity = resp.order.total_quantity;
            } else {
                 $ionicPopup.alert({
                  title: 'Error',
                  template: resp.message
                });
            }
          });

    };

    $scope.gotoSchedule = function() {

        if( $scope.washType == 'quick' && $scope.model.totalQuantity  > $scope.formData['express_quantity_limit']) {
            $ionicPopup.alert({
                 title: 'Error',
                 template: 'Max allowed quantity is: ' +  $scope.formData['express_quantity_limit'] 
               });
           return;
       }

        if($scope.model.totalQuantity == 0) {
             $ionicPopup.alert({
                  title: 'Error',
                  template: 'No items added to basket'
                });
            return;
        }

        $scope.formData.washType = $scope.washType;

        LM.postWashDetails($scope.washType)
          .then(function(resp){
              if(resp.success){
                if(resp.has_addons) {
                  $state.go('app.addons', {washType: $scope.washType});
                } else {
                  $state.go('app.washSchedule', {washType: $scope.washType});
                }
              } else {
                $ionicPopup.alert({
                  title: 'Error',
                  template: 'Error connecting the server. Please try again after sometime'
                });
              }
          }, function(error){
              console.error('Error submitting wash details.Error:' + error);
              $ionicPopup.alert({
                title: 'Error',
                template: 'Error connecting the server. Please try again after sometime'
              });
          });

        //$localStorage.setObject('lm.order.details', $scope.formData);

    };

    /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
   $scope.currentGroup = 'Men';

   $scope.toggleGroup = function(group) {
       if($scope.currentGroup == group) {
           $scope.currentGroup = '';
       } else {
          $scope.currentGroup = group;
       }

   };

   $scope.isGroupShown = function(group) {
       return group == $scope.currentGroup;
   };

})

.controller('AddonsCtrl', function($scope,
                                  $ionicPopup,
                                  $state,
                                  $stateParams,
                                  LM) {

    $scope.addons = [];
    $scope.orderTotal = 0;
    $scope.total = 0;
    $scope.addonUpdates = {};

    if($stateParams.washType == 'quick') {
        $scope.washType = 'quick';
    } else {
        $scope.washType = 'schedule';
    }
                                 
    LM.getAddons()
      .then(function(resp){
          $scope.addons = resp.addons;
          $scope.orderTotal = resp.total;
          $scope.updateTotal();
     });   

     $scope.updateAddon = function(addon, qty, comments) {

        var addonUpdate = $scope.addonUpdates[addon.id+"_"+addon.pricing_id];
        if(!addonUpdate) {
          addonUpdate = {}
        }
        if(qty) {
          addonUpdate['qty'] = qty;
        }
        if(comments) {
          addonUpdate['comments'] = comments;
        }
        $scope.addonUpdates[addon.id+"_"+addon.pricing_id] = addonUpdate;


     } 

    $scope.incr = function(index, parentIndex, addon){

        var qty = parseInt(addon.quantity);
        qty += 1;
        addon.quantity = qty;
        addons = this.addons[parentIndex];
        addons.addons[index] = addon;
        this.addons[parentIndex] = addons;
        $scope.updateTotal();
        $scope.updateAddon(addon, qty);

    } 

    $scope.decr = function(index, parentIndex, addon) {

        var qty = parseInt(addon.quantity);
        qty -= 1;
        if(qty < 0) { 
          qty = 0;
        }
        addon.quantity = qty;
        addons = this.addons[parentIndex];
        addons.addons[index] = addon;
        this.addons[parentIndex] = addons;
        $scope.updateTotal();
        $scope.updateAddon(addon, qty);
    }           

    $scope.updateTotal = function(){
        var total = 0;
        for(var i=0;i<this.addons.length;i++){
            for(var j=0;j<this.addons[i].addons.length;j++) {
                addon = this.addons[i].addons[j];
                total += addon.quantity * addon.price;
            }
        }
        $scope.total = $scope.orderTotal + total;
    }   

    $scope.updateComment = function(index, parentIndex, event) {

        addons = this.addons[parentIndex];
        addon = addons.addons[index];
        addon.comments = event.target.value;
        addons.addons[index] = addon;
        this.addons[parentIndex] = addons;
        $scope.updateAddon(addon, null, event.target.value);

    }

    $scope.gotoSchedule = function(){

          LM.postAddons($scope.addonUpdates).then(function(resp){
              if(resp.success) {
                  $state.go('app.washSchedule', {washType: $scope.washType});
              } else {
                  $ionicPopup.alert({
                    title: 'Error',
                    template: 'Error connecting to the server. Please try again after sometime'
                  });
              }
          }, function(error){
              console.error('Error posting slot details Error:' + error);
              $ionicPopup.alert({
                title: 'Error',
                template: 'Error connecting to the server. Please try again after sometime'
              });
          });
        
    }

})

.controller('WashScheduleCtrl', function($scope,
                                         $state,
                                         $stateParams,
                                         $ionicPopup,
                                         $ionicModal,
                                         $cordovaGeolocation,
                                         LM,
                                         Auth) {

  $scope.slotsInitialized = false;

  $scope.washType = $stateParams.washType;

  $scope.data = {
      pickup_slot: null,
      delivery_slot: null,
      special_instructions: ''
  };

  $scope.address = {
    first_name: "",
    last_name: "",
    address: "",
    locality_name: "",
    landmark: "",
    pincode: 0,
    errors: {}
  };

  //Possible that it can hold stale date especially slots. Always get data from server
  //var slotsData = $localStorage.getObject('lm.order.slots');
  var slotsData = {};

  $scope.selectData = {
      deliverySlots: [['Slot1', 'Slot1']]
  };

  if(angular.equals(slotsData, {})) {
    LM.getSlots($scope.washType)
      .then(function(resp){
        $scope.slotsData = resp;
        $scope.initSlots();
      });
  } else {
      $scope.slotsData = slotsData;
      $scope.initSlots();
  }

  $ionicModal.fromTemplateUrl('templates/addAddress.html', {
      scope: $scope,
      animation: 'slide-in-up'
  }).then(function(modal) {
      $scope.modal = modal;
  });

  $scope.initSlots = function(){
      //$scope.data.pickup_slot = $scope.slotsData.pickup_slots[0][0];
      //$scope.data.delivery_slot = $scope.slotsData.delivery_slots[$scope.data.pickup_slot][0];

      if($scope.washType == 'quick') {
          $scope.data.pickup_slot = $scope.slotsData.pickup_slots;
          $scope.data.delivery_slot = $scope.slotsData.delivery_slots;
      }

      $scope.slotsInitialized = true;

      if(!$scope.slotsData.address) {
        var posOptions = {timeout: 10000, enableHighAccuracy: false};
        $cordovaGeolocation
          .getCurrentPosition(posOptions)
          .then(function (position) {
            $scope.address.lat = position.coords.latitude,
              $scope.address.lng = position.coords.longitude
          }, function(err) {
            console.log('Error getting lat,lng.. using default');
            $scope.address.lat = 28.4406583;
            $scope.address.lng = 76.9873477;
          });

      }

  };

  $scope.updateDeliverySlot = function() {
        $scope.selectData.deliverySlots = $scope.slotsData.delivery_slots[$scope.data.pickup_slot[0]];
  };

  $scope.showAlert = function(message) {
    var alertPopup = $ionicPopup.alert({
      title: 'Error',
      template: message
    });

    alertPopup.then(function(res) {
      //console.log('Thank you');
    });

  };

  $scope.gotoPayment = function(){

      if(!$scope.slotsData.address) {
          $scope.showAlert('Please add a address');
          return false;
      }

      if(!$scope.data.delivery_slot || !$scope.data.pickup_slot) {
          $scope.showAlert('Please select pickup & delivery slot');
      }  else {

          if($scope.washType == 'schedule') {
              slots = {
                pickup_slot: $scope.data.pickup_slot[0],
                pickup_slot_display: $scope.data.pickup_slot[1],
                delivery_slot: $scope.data.delivery_slot,
                delivery_slot_display: $scope.slotsData.delivery_slots[$scope.data.pickup_slot[0]][1],
              }
          } else {

              slots =  {
                pickup_slot: $scope.slotsData.pickup_slots,
                pickup_slot_display: $scope.slotsData.pickup_slots,
                delivery_slot: $scope.slotsData.delivery_slots,
                delivery_slot_display: $scope.slotsData.delivery_slots
              }

          }

          LM.postSlotDetails(slots, $scope.data.special_instructions).then(function(resp){
              if(resp.success) {
                  $state.go('app.payment');
              } else {
                  $ionicPopup.alert({
                    title: 'Error',
                    template: 'Error connecting to the server. Please try again after sometime'
                  });
              }
          }, function(error){
              console.error('Error posting slot details Error:' + error);
              $ionicPopup.alert({
                title: 'Error',
                template: 'Error connecting to the server. Please try again after sometime'
              });
          });

      }

  };

  $scope.addAddress = function() {
    $scope.modal.show();
  };

  $scope.openModal = function(){
    $scope.modal.show();
  };

  $scope.closeModal = function(){
    $scope.modal.hide();
  };

  $scope.saveAddress = function() {

    LM.createAddress($scope.address)
      .then(function(resp){
          if(resp.success) {
            $scope.slotsData.address = resp.address;
            $scope.closeModal();
          } else {
            $scope.openModal();
            $scope.address.errors = resp.errors;
          }
      })

  };

})

.controller('PaymentCtrl', function($scope,
                                    $state,
                                    $ionicHistory,
                                    $ionicPopup,
                                    Auth,
                                    LM,
                                    $localStorage) {

    $scope.data = {
        coupon: "",
        couponApplied: false,
        order: {},
        paymentMethod:  'cod',
        paymentMessage: false
    };

    $scope.updateOrderDetails = function(){

      LM.getCurrentOrder()
        .then(function(resp){
          $scope.data.order = resp.order;
        });

      LM.getPaymentMessage()
        .then(function(resp){
            if(resp.has_message) {
                $scope.data.paymentMessage = resp.message;
            }
        })

    };

    $scope.updateOrderDetails();

    $scope.applyCoupon = function() {
      LM.applyCoupon($scope.data.coupon)
        .then(function(resp){
            if(resp.success) {
                $ionicPopup.alert({
                  title: 'Success',
                  template: 'Coupon applied successfully'
                });
                $scope.updateOrderDetails();
                $scope.data.couponApplied = true;
            }else {
                $ionicPopup.alert({
                  title: 'Error',
                  template: resp.message
                });
            }
        })
    };

    $scope.gotoReview = function() {
        $state.go('app.orderReview', {paymentMethod: $scope.data.paymentMethod});
    };

})


.controller('OrderReviewCtrl', function($scope,
                                        $state,
                                        $ionicHistory,
                                        $ionicPopup,
                                        $stateParams,
                                        Auth,
                                        LM,
                                        $localStorage,
                                        $cordovaInAppBrowser,
                                        $rootScope) {

    $scope.data = {
       order: {},
       paymentMethod:  $stateParams.paymentMethod || 'cod'
    };

    LM.getCurrentOrder()
      .then(function(resp){
        $scope.data.order = resp.order;
      });

    $scope.onPaymentError = function(){
        $ionicPopup.alert({
          title: 'Error',
          template: 'Error Making Payment. Please try again.'
        });
        $state.go('app.payment');
    };

    $scope.getParameterByName = function(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    };


    $scope.createOrder = function() {

        LM.createOrder($scope.data.paymentMethod)
          .then(function(resp){
              if(!resp.action || resp.action == 'complete-order') {
                  $ionicHistory.clearHistory();
                  $ionicHistory.clearCache();
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  $state.go('app.orderDetail', {orderId: resp.order_id});
              }
              if(resp.action == 'submit-pg-form') {

                var options = {
                  location: 'no',
                  clearcache: 'yes',
                  toolbar: 'no'
                };

                $cordovaInAppBrowser.open(resp.pg_url + '?token=' + Auth.getToken(), '_blank', options).
                then(function (event) {
                  // success
                })
                .catch(function (event) {
                  // error
                });

                // Once the InAppBrowser finishes loading
                $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {

                    if (event.url.search('/mapi/payment/response/') != -1) {
                      var order_id = parseInt($scope.getParameterByName('order_id', event.url));
                      if (order_id != -1) {
                        $cordovaInAppBrowser.close();
                        $ionicHistory.clearHistory();
                        $ionicHistory.clearCache();
                        $ionicHistory.nextViewOptions({
                          disableBack: true
                        });
                        $state.go('app.orderDetail', {orderId: order_id});
                      } else {
                        $cordovaInAppBrowser.close();
                        $scope.onPaymentError();
                      }

                    }

                });

                $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event){
                    $cordovaInAppBrowser.close();
                    $scope.onPaymentError();
                });

                $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event){
                });


              }
          });

    };

})


.controller('OrderDetailCtrl', function($scope, $state, $stateParams, $ionicPopup, Auth, LM) {

  $scope.data = {
      order: {}
  };

  LM.getOrderDetails($stateParams.orderId)
    .then(function(resp){
        $scope.data.order = resp.order;
    });

})

.controller('ManageAddressCtrl', function($scope, $ionicModal, LM) {

    $scope.address = {};
    $scope.errors = {};

    LM.getAddress()
    .then(function(resp){
        $scope.address = resp.address;
    });

    $ionicModal.fromTemplateUrl('templates/editAddress.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.updateAddress = function() {
        LM.updateAddress($scope.address)
          .then(function(resp){
              if(resp.success) {
                $scope.address = resp.address;
                $scope.closeModal();
              } else {
                $scope.openModal();
                $scope.errors = resp.errors;
              }
          })
    };

    $scope.editAddress = function() {
        $scope.modal.show();
    };

    $scope.openModal = function(){
        $scope.modal.show();
    };

    $scope.closeModal = function(){
        $scope.modal.hide();
    };


})

.controller('WalletCtrl', function($scope, $ionicPopup, $rootScope, $cordovaInAppBrowser, LM, Auth) {

  $scope.walletDetails = {
    balance: 0,
    entries: [],
    pg_url: ''
  };

  $scope.wallet = {
    fundAmount: 0
  };

  LM.getWalletDetails()
    .then(function(resp){
        $scope.walletDetails.balance = resp.balance;
        $scope.walletDetails.entries = resp.entries;
        $scope.walletDetails.pg_url = resp.pg_url;
    });


  $scope.onPaymentError = function(){
    $ionicPopup.alert({
      title: 'Error',
      template: 'Error Making Payment. Please try again.'
    });
    $state.go('app.mywallet');
  };

  $scope.getParameterByName = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  };


  $scope.fundWallet = function(){

      if( !$scope.wallet.fundAmount || $scope.wallet.fundAmount == 0) {
        $ionicPopup.alert({
          title: 'Error',
          template: 'Fund amount must be more than Rs.1.'
        });
        return;
      }
      var options = {
        location: 'no',
        clearcache: 'yes',
        toolbar: 'no'
      };

      $cordovaInAppBrowser.open($scope.walletDetails.pg_url + '?token=' + Auth.getToken() + '&amount=' + $scope.wallet.fundAmount,
                                '_blank',
                                options).
        then(function (event) {
          // success
          console.log('Wallet inappbrowser success.' + event);
        })
        .catch(function (event) {
          // error
          console.log('Wallet inappbrowser error.' + event);
        });

      // Once the InAppBrowser finishes loading
      $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {

        if (event.url.search('/mapi/payment/response/') != -1) {
          var added = parseInt($scope.getParameterByName('added', event.url));
          if (added != -1) {
            $cordovaInAppBrowser.close();
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go('app.mywallet');
          } else {
            $cordovaInAppBrowser.close();
            $scope.onPaymentError();
          }

        }

      });

      $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event){

          $cordovaInAppBrowser.close();
          $scope.onPaymentError();

      });

      $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event){
      });


  };

})
