#!/bin/bash
# Manually emulate ionic/cordova application
# Miroslav Masat 2014 

echo "Emulating..."
cd ./platforms/ios/build/emulator
var=$(pwd)

#ios-sim launch --devicetype="iPhone-5s, 9.3" "$var"/*.app
ios-sim launch --devicetype="iPad-Pro, 9.3" "$var"/*.app
